#!/bin/sh
source .env
git add .
git commit -am "update"
git push
yarn run build
rsync -avz --delete public/ ${USER}@${HOST}:~/${DIR}

exit 0

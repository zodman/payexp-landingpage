// See http://brunch.io for documentation.
exports.files = {
  javascripts: {joinTo: 'app.js'},
  stylesheets: {joinTo: 'app.css'}
};


exports.npm= {
        styles: {
                    'font-awesome': ['css/font-awesome.min.css'],
                    'bulma': ['css/bulma.css'],
                     'css-spaces': ["dist/spaces.min.css"],

                  }

}
exports.plugins= {
            copycat: {
                                  fonts: [
                                                                    "node_modules/font-awesome/fonts"
                                                                  ]
                                }
}
